# Default cipher reference source code
# Written by Anubhab Baksi <anubhab001@e.ntu.edu.sg>
# Hosted at https://bitbucket.org/anubhab001/default-cipher-public-code

LS_sbox = 0, 3, 7, 14, 13, 4, 10, 9, 12, 15, 1, 8, 11, 2, 6, 5
LS_inv_sbox = 0, 10, 13, 1, 5, 15, 14, 2, 11, 7, 6, 12, 8, 4, 3, 9
non_LS_sbox = 1, 9, 6, 15, 7, 12, 8, 2, 10, 14, 13, 0, 4, 3, 11, 5
non_LS_inv_sbox = 11, 0, 7, 13, 12, 15, 2, 4, 6, 1, 8, 14, 5, 10, 9, 3


player = 0, 33, 66, 99, 96, 1, 34, 67, 64, 97, 2, 35, 32, 65, 98, 3, 4, 37, 70, 103, 100, 5, 38, 71, 68, 101, 6, 39, 36, 69, 102, 7, 8, 41, 74, 107, 104, 9, 42, 75, 72, 105, 10, 43, 40, 73, 106, 11, 12, 45, 78, 111, 108, 13, 46, 79, 76, 109, 14, 47, 44, 77, 110, 15, 16, 49, 82, 115, 112, 17, 50, 83, 80, 113, 18, 51, 48, 81, 114, 19, 20, 53, 86, 119, 116, 21, 54, 87, 84, 117, 22, 55, 52, 85, 118, 23, 24, 57, 90, 123, 120, 25, 58, 91, 88, 121, 26, 59, 56, 89, 122, 27, 28, 61, 94, 127, 124, 29, 62, 95, 92, 125, 30, 63, 60, 93, 126, 31

inv_player = 0, 5, 10, 15, 16, 21, 26, 31, 32, 37, 42, 47, 48, 53, 58, 63, 64, 69, 74, 79, 80, 85, 90, 95, 96, 101, 106, 111, 112, 117, 122, 127, 12, 1, 6, 11, 28, 17, 22, 27, 44, 33, 38, 43, 60, 49, 54, 59, 76, 65, 70, 75, 92, 81, 86, 91, 108, 97, 102, 107, 124, 113, 118, 123, 8, 13, 2, 7, 24, 29, 18, 23, 40, 45, 34, 39, 56, 61, 50, 55, 72, 77, 66, 71, 88, 93, 82, 87, 104, 109, 98, 103, 120, 125, 114, 119, 4, 9, 14, 3, 20, 25, 30, 19, 36, 41, 46, 35, 52, 57, 62, 51, 68, 73, 78, 67, 84, 89, 94, 83, 100, 105, 110, 99, 116, 121, 126, 115

RC = 1, 3, 7, 15, 31, 62, 61, 59, 55, 47, 30, 60, 57, 51, 39, 14, 29, 58, 53, 43, 22, 44, 24, 48, 33, 2, 5, 11, 1, 3, 7, 15, 31, 62, 61, 59, 55, 47, 30, 60, 57, 51, 39, 14, 29, 58, 53, 43, 22, 44, 24, 48, 1, 3, 7, 15, 31, 62, 61, 59, 55, 47, 30, 60, 57, 51, 39, 14, 29, 58, 53, 43, 22, 44, 24, 48, 33, 2, 5, 11

def sbox_layer(pt, sbox):
	ct = [None]*len(pt)
	for i in range(len(pt)):
		ct[i] = sbox[pt[i]]
	return ct[:]


def bits_to_nibble(x):
	assert len(x) == 4
	y = x[0]*1 + x[1]*2 + x[2]*4 + x[3]*8
	return y

def bits_to_nibbles(x):
	y = []
	for i in range(len(x)//4):
		z = x[4*i : 4*i + 4]
		y.append(bits_to_nibble(z))
	return y 

def nibble_to_bits(x):
	y = []
	z = [zz == '1' for zz in (bin(x)[2:]).zfill(4)][::-1]
	y.extend(z)
	return y

def nibbles_to_bits(x):
	y = map(nibble_to_bits, x)
	return [item for sublist in y for item in sublist]

def nibble_encoding(a):
	assert [type(aa) == type(True) for aa in a]
	a_nibbles = bits_to_nibbles(a)

	b = []
	for aa in a_nibbles:
		b.append(bits_to_nibble(nibble_to_bits(aa)[::-1]))

	return nibbles_to_bits(b)

def key_update(key):
	## key update

	# Right rotate the entire key 20 steps
	temp_key = nibbles_to_bits(key)
	temp_key = temp_key[20:] + temp_key[:20]
	# Right rotate most significant 16 bits 1 step
	temp_key_16bit = temp_key[-16:]
	temp_key_16bit = nibble_encoding(temp_key_16bit)
	temp_key_16bit = temp_key_16bit[1:] + temp_key_16bit[:1]
	temp_key_16bit = nibble_encoding(temp_key_16bit)
	temp_key = temp_key[:-16] + temp_key_16bit
	
	assert len(temp_key) == 128
	key = bits_to_nibbles(temp_key)
	
	return key

def add_round_constants(state, rc):
	state[3] ^= (rc & 1)
	state[7] ^= ((rc >> 1) & 1) 
	state[11] ^= ((rc >> 2) & 1)
	state[15] ^= ((rc >> 3) & 1)
	state[19] ^= ((rc >> 4) & 1)
	state[23] ^= ((rc >> 5) & 1)
	state[127] ^= True 
	return state

def perm_layer(x, player):
	assert len(x) == 128
	y = [None]*len(x)
	for i in range(len(x)):
		y[player[i]] = x[i]
	return y[:]

def add_key(pt, key):
	return [p^k for p, k in zip(pt, key)]

def pretty_print(x):
	y = []
	for xx in x:
		y.append(hex(xx)[2:])
	return (''.join(y))[::-1]

def encryptNonLS(round_keys, pt, no_of_rounds=28, round_starting=0):

	for r in range(no_of_rounds):

		# sbox
		pt = sbox_layer(pt=pt, sbox=non_LS_sbox)

		# permutation
		pt = nibbles_to_bits(pt)
		pt = perm_layer(x=pt, player=player)

		# round constants
		pt = add_round_constants(pt, rc=RC[r + round_starting])
		pt = bits_to_nibbles(pt)

		# round key add
		round_key = round_keys[r + round_starting + 1]
		pt = add_key(pt, round_key)

	return pt

def encryptLS(round_keys, initial_key_add, pt, no_of_rounds=28, round_starting=0):

	# Initial add round key
	if initial_key_add:
		round_key = round_keys[0]
		pt = add_key(pt, round_key)

	for r in range(no_of_rounds):
		# sbox
		pt = sbox_layer(pt=pt, sbox=LS_sbox)
		
		# permutation
		pt = nibbles_to_bits(pt)
		pt = perm_layer(x=pt, player=player)

		## round constants
		pt = add_round_constants(pt, rc=RC[r + round_starting])
		pt = bits_to_nibbles(pt)
		
		# round key add
		round_key = round_keys[r + round_starting + 1]
		if (not initial_key_add) and (r == no_of_rounds - 1):
			pt = add_key(pt, round_key)

	return pt

def decryptNonLS(round_keys, pt, no_of_rounds=28, round_starting=0):

	for r in range(no_of_rounds)[::-1]:
		
		# round key add
		round_key = round_keys[r + round_starting + 1]
		pt = add_key(pt, round_key)
		
		# round constants
		pt = nibbles_to_bits(pt)
		pt = add_round_constants(pt, rc=RC[r + round_starting])
		
		# inv permutation
		pt = perm_layer(x=pt, player=inv_player)
		pt = bits_to_nibbles(pt)

		# inv sbox
		pt = sbox_layer(pt=pt, sbox=non_LS_inv_sbox)
		
		
	return pt

def decryptLS(round_keys, initial_key_add, pt, no_of_rounds=28, round_starting=0):

	for r in range(no_of_rounds)[::-1]:

		# round key add
		round_key = round_keys[r + round_starting + 1]
		if (not initial_key_add) and (r == no_of_rounds - 1):
			pt = add_key(pt, round_key)

		# round constants
		pt = nibbles_to_bits(pt)
		pt = add_round_constants(pt, rc=RC[r + round_starting])

		# inv permutation
		pt = perm_layer(x=pt, player=inv_player)
		pt = bits_to_nibbles(pt)

		# inv sbox
		pt = sbox_layer(pt=pt, sbox=LS_inv_sbox)
	
	# Final add round key
	if initial_key_add:
		round_key = round_keys[0]
		pt = add_key(pt, round_key)
		
	return pt

def encrypt(round_keys, pt, no_of_rounds):
	ct_inter = encryptLS(no_of_rounds=no_of_rounds[0], round_keys=round_keys[:], pt=pt[:], round_starting=0, initial_key_add=True)
	ct_inter = encryptNonLS(no_of_rounds=no_of_rounds[1], round_keys=round_keys[:], pt=ct_inter[:], round_starting=no_of_rounds[0])
	ct = encryptLS(no_of_rounds=no_of_rounds[0], round_keys=round_keys[:], pt=ct_inter[:], round_starting=no_of_rounds[0]+no_of_rounds[1], initial_key_add=False)
	return ct

def decrypt(round_keys, ct, no_of_rounds):
	pt_inter = decryptLS(no_of_rounds=no_of_rounds[0], round_keys=round_keys[:], pt=ct[:], round_starting=no_of_rounds[0]+no_of_rounds[1], initial_key_add=False)
	pt_inter = decryptNonLS(no_of_rounds=no_of_rounds[1], round_keys=round_keys[:], pt=pt_inter[:], round_starting=no_of_rounds[0])
	pt = decryptLS(no_of_rounds=no_of_rounds[0], round_keys=round_keys[:], pt=pt_inter[:], round_starting=0, initial_key_add=True)
	return pt
	
	
if __name__ == '__main__':
	
	key = [13,3,12,7, 1,4,5,8, 15,1,2,0, 5,6,4,4, 0,11,10,9 ,3,3,1,0, 11,14,13,11, 10,9,2,3]
	pt  = [1,13,4,9, 11,3,4,1, 5,2,5,1, 1,1,7,13, 4,3,4,1, 0,11,12,11, 11,12,13,15, 11, 14,12,0]

	print ('Key', pretty_print(key))
	print ('PT ', pretty_print(pt))
	
	assert len(pt) == 32
	assert len(key) == 32
	no_of_rounds_ls = 28
	no_of_rounds_nonls = 24

	round_keys = [key[:]]
	for i in range(no_of_rounds_ls*2 + no_of_rounds_nonls):
		round_keys.append(key_update(round_keys[i-1][:]))

	
	ct = encrypt(round_keys=round_keys[:], pt=pt[:], no_of_rounds=(no_of_rounds_ls, no_of_rounds_nonls))
	assert pt == decrypt(round_keys=round_keys[:], ct=ct[:], no_of_rounds=(no_of_rounds_ls, no_of_rounds_nonls))

	print ('CT ', pretty_print(ct))
